# cAtCDC

### Setup a development environment

Install `docker` and `docker-compose` using your distribution's documentation.
Also install `python3.7` and `poetry`. 

For those using Nix, everything is ready in the `shell.nix` file.

#### Initial setup

```sh
$ poetry install
$ cd docker/
$ ./gen_secrets.sh
$ docker-compose up --build
```

#### Regular usage

```sh
$ cd docker/
$ docker-compose up --build
```

#### Services

##### Adminer

```
URL: http://localhost:8010/
System: 'PostgreSQL'
User: 'catcdc_dev'
Password in: docker/secrets/postgres-passwd
Database: 'catcdc_dev'
```

##### Website

```
URL: http://localhost:8000/
To login with a user in your fixtures: http://localhost:8000/admin
To create a superuser:
$ cd docker && ./manage.sh createsuperuser
```

#### Formatting

We use `python/black` as a code formatter. Don't forget to format your code
before sharing it:

```
$ poetry run black .
```

#### Linting

We also use `pylint` to check the coding style. It is configured with
`gitlab-ci` and produce warnings in GitLab. To check if the project matches
`pylint`'s recommendations you can use the following command:

```
$ poetry run prospector --profile base
```

### Acknowledgements

Thanks to Marin 'mareo' Hannache for the `conf/env.py` file!

Thanks to Eliaz 'Kirikou' Pitavy for the javascript for ordering the votes!
