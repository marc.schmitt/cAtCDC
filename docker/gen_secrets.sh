#!/bin/sh

RAND_SECRET_NAMES="django-secret-key django-admin-passwd postgres-passwd epita-auth-key epita-auth-secret"

for name in ${RAND_SECRET_NAMES}; do
  DST=./secrets/$name
  tr -cd 'a-zA-Z0-9' < /dev/urandom | fold -w 32 | head -n 1 > ${DST}
done

# The following secrets have hardcoded values and are not modifiable for now
echo accessKey1 > ./secrets/s3-access
echo verySecretKey1 > ./secrets/s3-secret
