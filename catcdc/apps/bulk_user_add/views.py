from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.auth.models import Group
from django.shortcuts import render, redirect
from django.views.generic import TemplateView

from .forms import BulkUserAddForm


class BulkUserAddView(PermissionRequiredMixin, TemplateView):
    permission_required = "auth.add_user"
    permission_required = "auth.change_user"
    template_name = "bulk_user_add/form.html"

    # pylint:disable=unused-argument
    def get(self, request, *args, **kwargs):
        context = {
            "groups": Group.objects.all(),
            "form": BulkUserAddForm(),
        }
        return render(request, self.template_name, context)

    # pylint:disable=unused-argument,line-too-long
    def post(self, request, *args, **kwargs):
        form = BulkUserAddForm(request.POST)

        if form.is_valid():
            logins = form.cleaned_data["logins"].split("\n")
            for login in logins:
                login = login.lstrip().rstrip()
                user = get_user_model().objects.filter(username=login).first()
                if not user:
                    user = get_user_model().objects.create_user(
                        username=login, email=f"{login}@epita.fr", password=None
                    )

                form.cleaned_data["group"].user_set.add(user)
                user.save()

            return redirect("landing_page")

        context = {
            "form": form,
        }
        return render(request, self.template_name, context)
