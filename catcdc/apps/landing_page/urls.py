from django.urls import path

from .views import IndexView, ContributorsView

urlpatterns = [
    path("", IndexView.as_view(), name="landing_page"),
    path("contributors", ContributorsView.as_view(), name="contributors"),
]
