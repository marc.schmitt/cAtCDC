from django.conf import settings
from django.utils import timezone


def is_submission_started():
    return settings.SUBMISSION_START < timezone.now()


def is_submission_ended():
    return settings.SUBMISSION_END < timezone.now()


def is_vote_started():
    return settings.VOTE_START < timezone.now()


def is_vote_ended():
    return settings.VOTE_END < timezone.now()


def has_group(user, group_name):
    return user.groups.filter(name=group_name).exists()
