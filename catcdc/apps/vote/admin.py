from django.contrib import admin
from django.contrib.admin.options import ModelAdmin

from .models import Submission, Vote


@admin.register(Submission)
class SubmissionAdmin(ModelAdmin):
    list_display = ["submitter", "cat_name"]
    list_filter = ["submitter"]


@admin.register(Vote)
class VoteAdmin(ModelAdmin):
    list_display = ["voter", "submission", "weight"]
    list_filter = ["voter", "submission"]
