from django.db import models
from django.conf import settings


class Submission(models.Model):
    # pylint:disable=line-too-long
    submitter = models.OneToOneField(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="submitter"
    )
    cat_name = models.CharField(max_length=255)
    presentation = models.TextField(max_length=500)
    image_1 = models.ImageField()
    image_2 = models.ImageField()
    image_3 = models.ImageField()
    image_4 = models.ImageField()
    image_5 = models.ImageField()

    def __str__(self):
        return f"{self.submitter.username}: {self.cat_name}"


class Vote(models.Model):
    # pylint:disable=line-too-long
    voter = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="voter"
    )
    submission = models.ForeignKey(
        Submission, on_delete=models.CASCADE, related_name="submission"
    )
    weight = models.IntegerField()

    # pylint:disable=line-too-long
    def __str__(self):
        return f"{self.voter.username}: {self.weight} - {self.submission.cat_name}"
