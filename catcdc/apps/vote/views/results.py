from django.shortcuts import render
from django.views.generic import TemplateView

from common.util import (
    is_submission_started,
    is_submission_ended,
    is_vote_started,
    is_vote_ended,
)

from ..models import Submission, Vote


class ResultsView(TemplateView):
    template_name = "vote/results.html"

    def get(self, request, *args, **kwargs):
        context = {
            "is_submission_started": is_submission_started(),
            "is_submission_ended": is_submission_ended(),
            "is_vote_started": is_vote_started(),
            "is_vote_ended": is_vote_ended(),
        }

        votes = Vote.objects.all()
        nb_submissions = Submission.objects.count()

        results = []
        for submission in Submission.objects.all():
            submission_votes = votes.filter(submission=submission)
            score = 0
            top_vote = 0

            for vote in submission_votes:
                score += vote.weight
                if vote.weight == nb_submissions:
                    top_vote += 1
            results.append([submission, score, top_vote])
        results.sort(key=lambda s: (s[1]), reverse=True)
        context["results"] = results

        context["total_nb_votes"] = len(votes.distinct("voter"))

        return render(request, self.template_name, context)
