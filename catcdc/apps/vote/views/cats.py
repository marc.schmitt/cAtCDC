from random import shuffle

from django.shortcuts import render
from django.views.generic import TemplateView

from common.util import (
    is_submission_started,
    is_submission_ended,
    is_vote_started,
    is_vote_ended,
)

from ..models import Submission


class CatsView(TemplateView):
    template_name = "vote/cats.html"

    def get(self, request, *args, **kwargs):
        context = {
            "is_submission_started": is_submission_started(),
            "is_submission_ended": is_submission_ended(),
            "is_vote_started": is_vote_started(),
            "is_vote_ended": is_vote_ended(),
        }

        cats = Submission.objects.all()
        cats = list(cats)
        shuffle(cats)
        context["cats"] = cats

        return render(request, self.template_name, context)
