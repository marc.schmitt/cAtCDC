from random import shuffle

from django.conf import settings
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.shortcuts import render, redirect
from django.views.generic import TemplateView

from common.util import is_vote_started, is_vote_ended

from ..models import Submission, Vote
from ..forms import VoteForm


class VoteView(PermissionRequiredMixin, TemplateView):
    permission_required = ("vote.add_vote", "vote.change_vote")
    template_name = "vote/vote.html"

    # pylint:disable=unused-argument
    def get(self, request, *args, **kwargs):
        context = {
            "is_vote_started": is_vote_started(),
            "is_vote_ended": is_vote_ended(),
            "vote_start": settings.VOTE_START.isoformat(),
            "vote_end": settings.VOTE_END.isoformat(),
            "form": VoteForm(),
        }

        cats = list(Submission.objects.all())
        shuffle(cats)
        context["cats"] = cats

        return render(request, self.template_name, context)

    # pylint:disable=unused-argument
    def post(self, request, *args, **kwargs):
        form = VoteForm(request.POST)
        if form.is_valid() and is_vote_started() and not is_vote_ended():
            submissions = Submission.objects.all()
            current_votes = Vote.objects.filter(voter=request.user).delete()
            vote = form.cleaned_data.get("choices").split(",")[1:]
            n = len(vote)
            for i in range(n):
                Vote(
                    voter=request.user,
                    submission=submissions.get(pk=vote[i]),
                    weight=(n - i),
                ).save()
            return redirect("vote:results")

        context = {
            "is_vote_started": is_vote_started(),
            "is_vote_ended": is_vote_ended(),
            "vote_start": settings.VOTE_START.isoformat(),
            "vote_end": settings.VOTE_END.isoformat(),
        }
        cats = list(Submission.objects.all())
        shuffle(cats)
        context["cats"] = cats
        context["form"] = VoteForm()
        return render(request, self.template_name, context)
