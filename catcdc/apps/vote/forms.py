from django import forms

from .models import Submission


class SubmissionForm(forms.ModelForm):
    class Meta:
        model = Submission
        fields = [
            "cat_name",
            "presentation",
            "image_1",
            "image_2",
            "image_3",
            "image_4",
            "image_5",
        ]


class VoteForm(forms.Form):
    choices = forms.CharField(max_length=255, widget=forms.HiddenInput())
